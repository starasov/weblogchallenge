## Results v3

* No 4xx and 5xx status codes
* No web crawlers and empty user agents
* Excluded API and WordPress content requests
* No user sessions with one unique visit

### 30 Minutes

Average session time: '209' second(s)
Average session visits: '6'
The most engaged users: 'List(125.19.44.66, 192.8.190.10, 180.211.69.209, 122.15.156.64, 103.29.159.138, 125.20.39.66, 203.191.34.178, 180.151.80.140, 103.29.159.186, 125.20.9.248)'

### 20 Minutes

Average session time: '170' second(s)
Average session visits: '6'
The most engaged users: 'List(125.19.44.66, 192.8.190.10, 180.211.69.209, 122.15.156.64, 103.29.159.138, 125.20.39.66, 203.191.34.178, 180.151.80.140, 103.29.159.186, 125.20.9.248)'

### 15 minutes

Average session time: '133' second(s)
Average session visits: '6'
The most engaged users: 'List(125.19.44.66, 192.8.190.10, 180.211.69.209, 122.15.156.64, 103.29.159.138, 125.20.39.66, 203.191.34.178, 180.151.80.140, 103.29.159.186, 103.29.159.213)'