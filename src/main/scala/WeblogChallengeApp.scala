import org.apache.spark.{SparkConf, SparkContext}
import org.joda.time.Minutes

/**
  * Notes:
  *
  * 1. Check cumulative calculations in Spark -
  * https://databricks.gitbooks.io/databricks-spark-reference-applications/content/logs_analyzer/chapter1/total.html
  *
  * 2. How/where do we want to store results of the calculation. As we may be in the middle of a transformation
  * pipeline.
  *
  * 3. We can identify sessions first and then run analysis queries based on the intermediate results if we
  * want to answer more questions. We may want to take a look at SQL context variant of the problem..
  *
  * 4. Confirm "production" dataset sizes. How much and how often.
  *
  * 5. "Determine unique URL visits per session. To clarify, count a hit to a unique URL only once per session." -
  * unclear how to present results. For now we can the average number of visits per session. We also need to
  * define "uniqueness" of the URL.
  *
  * 6. For fine-tuning of execution see - http://spark.apache.org/docs/latest/configuration.html#application-properties
  *
  * 7. "Find the most engaged users, ie the IPs with the longest session times" - requirement can be interpreted
  * in two possible ways: total time spent by user or actual longest session per user. Both can indicate user
  * engagement.
  *
  * 8. Implementation does not include any additional infrastructure specific code or decisions such as
  * configuration, deployment, monitoring, etc. We may need more context than provided in the requirements to
  * make decision on this side.
  *
  * 9. http://apachesparkbook.blogspot.com/search/label/a72%7C%20reduceByKey%28%29
  *
  * 10. Unclear whether we need to exclude 3xx and /papi/ requests.
  *
  * 11. Some knowledge of how paytm.com is built may help to reduce the noise. For example
  * on products page they have infinite scrolling with XHR requests for each new portion loaded
  *
  * 12. We may want to compress/hash urls as we only care about uniqueness. This may reduce
  * heap memory usage
  *
  * 13. Client IP can be also compressed into 1 int value:
  * 10.10.10.1 is: (10 << 24) + (10 << 16) + (10 << 8) + 1 = 168430081
  *
  * 14. Have integration tests for smaller datasets.
  */
object WeblogChallengeApp {
  def main(args: Array[String]) {
    val logFile = "/Users/starasov/Projects/weblogchallenge/data/2015_07_22_mktplace_shop_web_log_sample.log.gz"

    val conf = new SparkConf()
      .setAppName("WeblogChallenge")

    val context = new SparkContext(conf)

    val records = context.textFile(logFile, 2).cache()
      .map(entry => Record.parse(entry))
      .filter(Record.excludedMethods("HEAD", "POST"))
      .filter(Record.excludedStatuses("4\\d\\d", "5\\d\\d"))
      .filter(Record.excludedUrls("/wp-content/", "/papi/"))
      .filter(Record.excludedUserAgents("-", "Googlebot", "WordPress", "Yahoo", "Bing"))

    val sessions = records.map(record => (record.clientIp, List(Session(record))))
      .reduceByKey(Session.combine(Minutes.minutes(15)))
      .mapValues(_.filter(_.urls.size > 1))
      .filter(_._2.nonEmpty)

    val averageSessionTime = sessions
      .flatMap(_._2.map(_.durationSeconds)).mean().round

    val averageSessionVisits = sessions
      .flatMap(_._2.map(_.urls.size)).mean().round

    val topUsers = sessions
      .sortBy(_._2.map(_.durationSeconds).max, ascending = false)
      .keys.take(10).toList

    println(s"Average session time: '$averageSessionTime' second(s)")
    println(s"Average session visits: '$averageSessionVisits'")
    println(s"The most engaged users: '$topUsers'")
  }
}