import java.net.URL

import com.github.nscala_time.time.Imports._


/**
  * Parse log entry line and returns record instance.
  *
  * 1. For log entry format see http://docs.aws.amazon.com/ElasticLoadBalancing/latest/DeveloperGuide/access-log-collection.html#access-log-entry-format
  * BTW: Additional fields for access log entries Added the user_agent, ssl_cipher, and ssl_protocol fields. For more information, see Access Log Files.
  * May 18, 2015.
  *
  * 2. Some elements appear in the log out of order.
  *
  * 3. Clean up inputs - for URLs we may need to remove ports and query parameters as
  * these may not necessarily be unique.
  *
  * 4. Handle HTTP vs HTTPS urls - as protocol change still makes it the same visit
  *
  * 5. Check ports - port change likely to occur only for HTTP -> HTTPS change
  *
  * 6. HTTP status codes - which ones do we count and which ones should we ignore
  *
  * 7. Make sure date format is consistent
  *
  * 8. Write more unit tests
  *
  * 9. Changing query parameters order does't make a unique visit
  *
  * 10. Changing subdomain may not necessarily make a unique visit.
  * For example: www.paytm.com, paytm.com, m.paytm.com
  *
  * 11. Consider adding some traceablity for records as we may want to know where to
  * find the record if invalid. Line number, unique ID, etc.
  *
  * 12. Ignore web crawlers - Googlebot, bingbot.
  *
  * 13. Ignore "technical" query paramaters? For example: XXX?channel=web&version=2
  *
  * 14 From top IPs these are quite suspicious
  *
  * * 119.81.61.166 - so far seems to be a proxy server
  * * 106.186.23.95 - so far seems to be a proxy server
  * * 220.226.206.7 - all calls to https://paytm.com:443/recharges with no user agent
  * * 54.251.151.39 - all calls to https://paytm.com:443/shop with no furter navigation
  *
  * May need to check others in top 10.
  *
  * 15. Another extremely frequent URL is: https://paytm.com/papi/v1/promosearch/...
  * It maybe something used by 3d parties to promote codes.
  *
  * 16. IP address whitelist/blacklist
  *
  * 17. http://link.springer.com/chapter/10.1007%2F0-387-23152-8_50#page-4
  *
  * 18. Matching of entries with a regex for 10x slower than working with splits.
  *
  * 19. What does it mean when user agent is blank (-)? Should we ignore these requests?
  *
  * 20. Check if there is a need to validate log entries format.
  */
object Record {

  def parse(logEntry: String): Record = {
    val tokens = logEntry.split("\"")

    val headerTokens = tokens(0).split(" ")
    val dateTime = new DateTime(headerTokens.head)
    val clientIp = headerTokens(2).split(":").head
    val statusCode = headerTokens(7)

    val requestTokens = tokens(1).split(" ")
    val method = requestTokens(0)
    val url = new URL(requestTokens(1))
    val query = if (url.getQuery == null) "" else url.getQuery
    val sanitizedUrl = url.getHost + url.getPath + query

    val userAgent = tokens(3)

    new Record(dateTime, clientIp, statusCode, method, sanitizedUrl, userAgent)
  }

  def excludedMethods(blacklist: String*)(record: Record): Boolean =
    !blacklist.contains(record.method)

  def excludedUrls(blacklist: String*)(record: Record): Boolean =
    !blacklist.exists(record.url.contains(_))

  def excludedUserAgents(blacklist: String*)(record: Record): Boolean =
    !blacklist.exists(record.userAgent.contains(_))

  def excludedStatuses(blacklist: String*)(record: Record): Boolean =
    !blacklist.exists(record.statusCode.matches)
}

case class Record(timestamp: DateTime,
                  clientIp: String,
                  statusCode: String,
                  method: String,
                  url: String,
                  userAgent: String)

