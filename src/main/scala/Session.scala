import com.github.nscala_time.time.Imports._
import org.joda.time.{Minutes, Seconds}

object Session {

  def combine(window: Minutes)(left: List[Session], right: List[Session]): List[Session] = {
    (left ::: right).sortBy(_.startTime.getMillis).foldRight(List[Session]()) {
      case (s, x :: xs) if s.canCombine(x, window) => s.combine(x) :: xs
      case (s, x :: xs) => s :: x :: xs
      case (s, _) => List(s)
    }
  }

  def apply(record: Record): Session =
    new Session(Set(record.url), record.timestamp, record.timestamp)
}

case class Session(urls: Set[String],
                   startTime: DateTime,
                   endTime: DateTime) {

  def canCombine(other: Session, window: Minutes): Boolean =
    isOverlap(other) || isSameSession(other, window)

  def combine(other: Session): Session =
    Session(this.urls ++ other.urls,
      if (this.startTime.isBefore(other.startTime)) this.startTime else other.startTime,
      if (this.endTime.isAfter(other.endTime)) this.endTime else other.endTime)

  def isBefore(other: Session): Boolean =
    this.endTime.isBefore(other.startTime)

  def isOverlap(other: Session): Boolean =
    this.startTime.compareTo(other.endTime) <= 0 && this.endTime.compareTo(other.startTime) >= 0

  def isSameSession(other: Session, window: Minutes): Boolean = {
    val left = if (this.isBefore(other)) this else other
    val right = if (this.isBefore(other)) other else this
    Minutes.minutesBetween(left.endTime, right.startTime).getMinutes < window.getMinutes
  }

  def durationSeconds: Int = {
    Seconds.secondsBetween(this.startTime, this.endTime).getSeconds
  }
}