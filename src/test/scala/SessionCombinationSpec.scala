import org.joda.time.Minutes
import org.scalatest._

class SessionCombinationSpec extends FlatSpec with Matchers with Fixtures {
  "Sessions combination" should "respect overlapping sessions" in {
    val left = List(aSession(start, start.plusMinutes(10)))
    val right = List(aSession(start.plusMinutes(5), start.plusMinutes(20)))
    val combined = Session.combine(Minutes.minutes(10))(left, right)

    combined should be(List(aSession(start, start.plusMinutes(20))))
  }

  it should "respect inactivity window" in {
    val left = List(aSession(start, start.plusMinutes(10)))
    val right = List(aSession(start.plusMinutes(15), start.plusMinutes(20)))
    val combined = Session.combine(Minutes.minutes(10))(left, right)

    combined should be(List(aSession(start, start.plusMinutes(20))))
  }

  it should "enforce ordering" in {
    val left = List(aSession(start, start.plusMinutes(10)))
    val right = List(aSession(start.plusMinutes(30), start.plusMinutes(40)))
    val combined = Session.combine(Minutes.minutes(10))(right, left)

    combined should be(left ++ right)
  }

  it should "combine empty sessions" in {
    val left = List(aSession(start, start.plusMinutes(10)))
    val combined = Session.combine(Minutes.minutes(10))(left, List())

    combined should be(left)
  }

  it should "combine sessions" in {
    val left = List(
      aSession(start, start.plusMinutes(10)),
      aSession(start.plusMinutes(30), start.plusMinutes(40)))

    val right = List(
      aSession(start.plusMinutes(5), start.plusMinutes(25)),
      aSession(start.plusMinutes(60), start.plusMinutes(70)))

    val combined = Session.combine(Minutes.minutes(10))(left, right)

    combined should be(List(
      aSession(start, start.plusMinutes(40)),
      aSession(start.plusMinutes(60), start.plusMinutes(70))))
  }
}