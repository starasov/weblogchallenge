import org.joda.time.DateTime

trait Fixtures {

  def start: DateTime =
    DateTime.now().withTimeAtStartOfDay()

  def aSession(from: DateTime, to: DateTime): Session =
    Session(Set(), from, to)
}
