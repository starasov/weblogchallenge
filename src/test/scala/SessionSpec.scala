import org.joda.time.Minutes
import org.scalatest._

class SessionSpec extends FlatSpec with Matchers with Fixtures {
  "A Session" should "be before another session" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(20), start.plusMinutes(30))
    left.isBefore(right) should be(true)
  }

  "A Session" should "not be before another session when overlap" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(5), start.plusMinutes(20))
    left.isBefore(right) should be(false)
  }

  "Sessions" should "not overlap when intervals are distinct" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(11), start.plusMinutes(20))

    left.isOverlap(right) should be(false)
    right.isOverlap(left) should be(false)
  }

  "Sessions" should "overlap when intervals intersect" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(5), start.plusMinutes(20))

    left.isOverlap(right) should be(true)
    right.isOverlap(left) should be(true)
  }

  "Sessions" should "overlap when one interval is inside another" in {
    val left = aSession(start, start.plusMinutes(20))
    val right = aSession(start.plusMinutes(5), start.plusMinutes(10))

    left.isOverlap(right) should be(true)
    right.isOverlap(left) should be(true)
  }

  "Sessions" should "be considered the same when apart for less than inactivity window" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(20), start.plusMinutes(30))

    left.isSameSession(right, Minutes.minutes(15)) should be(true)
    right.isSameSession(left, Minutes.minutes(15)) should be(true)
  }

  "Sessions" should "be considered distinct when apart for more than inactivity window" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(26), start.plusMinutes(35))

    left.isSameSession(right, Minutes.minutes(15)) should be(false)
    right.isSameSession(left, Minutes.minutes(15)) should be(false)
  }

  "Sessions" should "be combined when overlap" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(5), start.plusMinutes(20))

    left.canCombine(right, Minutes.minutes(15)) should be(true)
    right.canCombine(left, Minutes.minutes(15)) should be(true)
  }

  "Sessions" should "be combined when apart for less than inactivity window" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(11), start.plusMinutes(20))

    left.canCombine(right, Minutes.minutes(15)) should be(true)
    right.canCombine(left, Minutes.minutes(15)) should be(true)
  }

  "Sessions" should "not be combined when apart for more than inactivity window" in {
    val left = aSession(start, start.plusMinutes(10))
    val right = aSession(start.plusMinutes(30), start.plusMinutes(40))

    left.canCombine(right, Minutes.minutes(15)) should be(false)
    right.canCombine(left, Minutes.minutes(15)) should be(false)
  }

  "Sessions" should "be combined properly" in {
    val left = Session(Set("paytm1"), start, start.plusMinutes(10))
    val right = Session(Set("paytm2"), start.plusMinutes(11), start.plusMinutes(20))

    left.combine(right) should be(Session(Set("paytm1", "paytm2"), start, start.plusMinutes(20)))
  }

  "Session duration" should "be calculated properly" in {
    val session = aSession(start, start.plusMinutes(5))
    session.durationSeconds should be(300)
  }
}