import com.github.nscala_time.time.Imports._
import org.scalatest._

class RecordSpec extends FlatSpec with Matchers with Fixtures {
  "A Record" should "be parsed from a valid log entry" in {
    val record = Record.parse("2015-07-22T09:01:49.891195Z marketpalce-shop 14.102.53.58:4637 10.0.6.158:80 0.000024 0.017911 0.000016 406 406 76 188 \"POST https://paytm.com:443/shop/cart HTTP/1.1\" \"Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36\" ECDHE-RSA-AES128-GCM-SHA256 TLSv1.2")

    record.timestamp should be(new DateTime("2015-07-22T09:01:49.891195Z"))
    record.clientIp should be("14.102.53.58")
    record.statusCode should be("406")
    record.method should be("POST")
    record.url should be("paytm.com/shop/cart")
    record.userAgent should be("Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36")
  }

  "Record url" should "not include protocol when parsed" in {
    val record = Record.parse("2015-07-22T09:00:28.387519Z marketpalce-shop 203.196.206.174:36859 10.0.6.199:81 0.000022 0.000313 0.000037 301 301 0 178 \"GET http://www.paytm.com:80/ HTTP/1.1\" \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36\" - -")
    record.url should be("www.paytm.com/")
  }

  "Record url" should "not include port when parsed" in {
    val record = Record.parse("2015-07-22T09:00:28.387519Z marketpalce-shop 203.196.206.174:36859 10.0.6.199:81 0.000022 0.000313 0.000037 301 301 0 178 \"GET https://www.paytm.com:443/ HTTP/1.1\" \"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36\" - -")
    record.url should be("www.paytm.com/")
  }

  "Method exclusion" should "filter records" in {
    val records = List(
      Record(start, "8.8.8.8", "200", "GET", "paytm.com/", "-"),
      Record(start, "8.8.8.8", "200", "POST", "paytm.com/", "-")
    ).filter(Record.excludedMethods("POST"))

    records should be(List(Record(start, "8.8.8.8", "200", "GET", "paytm.com/", "-")))
  }
}